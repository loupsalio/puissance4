class game {
    _grid = null
    _width = null
    _height = null
    _player = 1
    _row = 0
    _col = 0
    _status = true
    _color_p1 = color(255, 0, 0)
    _color_p2 = color(255, 255, 0)
    _animation = false
    _frame = 0

    constructor(width, height, rowN = 7, colN = 6, color_p1 = color(255, 0, 0), color_p2 = color(255, 255, 0)) {
        this._grid = new Array(rowN).fill(0).map(() => new Array(colN).fill(0));
        this._width = width
        this._height = height
        this._row = rowN
        this._col = colN
        this._color_p1 = color_p1
        this._color_p2 = color_p2
    }

    debugGrid() {
        console.table(this._grid)
    }

    drawGrid(mouseX, v, b) {
        for (let y = 0; y < this._row; y++) {
            const row = this._grid[y];
            for (let x = 0; x < row.length; x++) {
                const tile = row[x];
                const left = (this._width / row.length / 2) + x * (this._width / row.length)
                const top = (this._height / this._row / 2) + y * (this._height / (this._row))
                const rayon = (this._width / row.length) * 0.6
                // v.html(round((mouseX / (this._width / row.length))))

                fill(color(255, 255, 255))
                // if (floor((mouseX / (this._width / row.length))) === x)
                //     fill(color(255, 255, 204))
                let pos = this.getFreePos(mouseX)
                if (!this._animation && this._status && pos && pos.x == x && pos.y == y)
                    fill(color(229, 255, 204))
                if (tile == 1 || tile == 5)
                    fill(this._color_p1)
                else if (tile == 2 || tile == 3)
                    fill(this._color_p2)
                ellipse(left, top, rayon)
                this.checkIfAnim()
                this.checkWin(v, b)
            }
        }
    }

    checkIfAnim() {
        if (floor(frameCount / 4) == this._frame)
            return
        this._frame = floor(frameCount / 4)
        let posP1 = this.findValue(6)
        if (posP1) {
            let posP1_5 = this.findValue(5)
            this._grid[posP1_5.y][posP1_5.x] = 0
            this._grid[posP1_5.y + 1][posP1_5.x] = 5
        }
        else {
            let posP1_5 = this.findValue(5)
            if (posP1_5) {
                this._grid[posP1_5.y][posP1_5.x] = 1
                this._animation = false
            }
        }
        let posP2 = this.findValue(4)
        if (posP2) {
            let posP2_5 = this.findValue(3)
            this._grid[posP2_5.y][posP2_5.x] = 0
            this._grid[posP2_5.y + 1][posP2_5.x] = 3
        }
        else {
            let posP2_5 = this.findValue(3)
            if (posP2_5) {
                this._grid[posP2_5.y][posP2_5.x] = 2
                this._animation = false
            }
        }
    }

    findValue(x) {
        for (let i = 0; i < this._row; i++) {
            for (let o = 0; o < this._col; o++) {
                if (this._grid[i][o] == x)
                    return ({ x: o, y: i })
            }
        }
    }

    getFreePos(mouseX) {
        const col = floor((mouseX / (this._width / this._col)))
        for (let index = this._row - 1; index >= 0; index--) {
            const element = this._grid[index];
            if (element[col] == 0)
                return { x: col, y: index };
        }
    }

    click(mouseX) {
        if (this._animation)
            return
        this._animation = true

        let pos = this.getFreePos(mouseX)
        // console.log(pos, pos.x, pos.y);
        if (this._status && typeof (pos) !== 'undefined' && typeof (pos.x) !== 'undefined' && typeof (pos.y) !== 'undefined') {
            // this._grid[pos.y][pos.x] = this._player
            this._player = (this._player == 1 ? 2 : 1)
            this._grid[0][pos.x] = (this._player == 1 ? 3 : 5)
            this._grid[pos.y][pos.x] = (this._player == 1 ? 4 : 6)
            // console.log(this._player);
        }
    }

    checkRight(x, y) {
        let init = this._grid[y][x]
        let p = true
        for (let d = 0; d < 4; d++) {
            if (!this._grid[y][x + d] || this._grid[y][x + d] != init || this._grid[y][x + d] == 0)
                p = false
        }
        return p
    }

    checkBottom(x, y) {
        let init = this._grid[y][x]
        let p = true
        for (let d = 0; d < 4; d++) {
            if (!this._grid[y + d] || this._grid[y + d][x] != init || this._grid[y + d][x] == 0)
                p = false
        }
        return p
    }

    checkDiagR(x, y) {
        let init = this._grid[y][x]
        let p = true
        for (let d = 0; d < 4; d++) {
            if (!this._grid[y + d] || !this._grid[y + d][x + d] || this._grid[y + d][x + d] != init || this._grid[y + d][x + d] == 0)
                p = false
        }
        return p
    }

    checkDiagL(x, y) {
        let init = this._grid[y][x]
        let p = true
        for (let d = 0; d < 4; d++) {
            if (!this._grid[y + d] || !this._grid[y + d][x - d] || this._grid[y + d][x - d] != init || this._grid[y + d][x - d] == 0)
                p = false
        }
        return p
    }


    checkWin(v, b) {
        if (!this._status)
            return
        for (let i = 0; i < this._row; i++) {
            for (let o = 0; o < this._col; o++) {
                if (this.checkRight(o, i) || this.checkBottom(o, i) || this.checkDiagR(o, i) || this.checkDiagL(o, i)) {
                    v.html("Player " + (this._grid[i][o] == 1 ? "Red" : "Yellow") + " won !");
                    this._status = false
                    this._animation = false
                    b.show()
                }
            }
        }
    }

    reset(v, b) {
        for (let i = 0; i < this._row; i++) {
            for (let o = 0; o < this._col; o++) {
                this._grid[i][o] = 0
            }
        }
        this._status = true
        this._player = 1
        this._animation = false
        b.hide()
        v.html('')
    }

}
