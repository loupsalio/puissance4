function setup() {
  canvas = createCanvas(1000, 800);

  myGame = new game(width, height, 7, 6);
  myGame.debugGrid();

  value = createP("")
  restart = createButton('Restart');
  restart.hide()
  restart.position(19, 50);
  restart.mousePressed(() => { myGame.reset(value, restart) });
  frameRate(45);
}

function draw() {
  clear()
  background(color(0, 0, 255));
  if (myGame) {
    myGame.drawGrid(mouseX, value, restart)
  }
}

function mouseClicked() {
  myGame.click(mouseX)
  // myGame.debugGrid()
}
